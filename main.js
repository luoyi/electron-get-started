var electron = require('electron')
var app = electron.app
var BrowserWindow = electron.BrowserWindow

app.on('ready', function () {
    var mainWindow = new BrowserWindow({
        width: 800,
        height: 600
    })

    // mainWindow.loadURL('http://www.baidu.com')
    mainWindow.loadURL('file://' + __dirname + '/index.html')

    // 不加任何参数，可以在本窗口内打开dev tool
    // mainWindow.webContents.openDevTools()

    // 加上参数，可以在新窗口中打开dev tool
    mainWindow.webContents.openDevTools({detach: true})
})